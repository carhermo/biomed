""" Tarea de Ciencia de datos para Biomedicina
Carlos Hernani Morales
gitlab::carhermo
correo::carhermo@protonmail.com;carhermo@alumni.uv.es
2019/2020

Clasificación de Leucemias

Python 3.7.5
Linter used: pycodestyle """


# Basic libraries
import numpy as np
import pandas as pd
from collections import Counter
# Visualization Libraries
import matplotlib.pyplot as plt
import seaborn as sns
# Preprocessing/Feature extraction libraries
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
# Metrics Libraries
from sklearn.metrics import classification_report
# Balancing data
from imblearn.over_sampling import SMOTE, SVMSMOTE, ADASYN, RandomOverSampler
# Modelos
from models import mod


class genetic:
    """
    Class to classify between two cases of leukemia, as seen
    in Golub et al. 1999 Oct 15;286(5439):531-7.
    doi: 10.1126/science.286.5439.531.
    """
    def __init__(self):
        self.data = None
        self.description = pd.read_csv("./data/golub_names.csv")
        self.labels = ['ALL', 'AML']
        self.xtrain = {}  # diccionarios con oir, bal, std, pca
        self.xtest = {}
        self.ytrain = {}
        self.ytest = {}
        self.historial = "Objeto Inicializado.\n"
        self.models = {
            "zero-rules", "logreg", "svm", "rf", "gbc", "xgb"
            }
        self.metrics = {}

    def show_history(self):
        """
        Muestra el historial de operaciones
        llevadas a cabo.
        """
        print(self.historial)

    def load_data(self, str_file_path):
        """
        Carga los datos de golub procedentes de la
        libreria multtest de R.
        """
        data_original = pd.read_csv(str_file_path)
        data_transposed = data_original.transpose()
        data_transposed = data_transposed.reset_index()
        data_transposed = data_transposed.drop("index", axis=1)
        data_transposed.columns = [
            'G' + str(ind) for ind in data_transposed.columns]
        data_transposed.insert(0, "class", pd.read_csv(
                            "./data/golub_cl.csv"
                            ).loc[:, 'class'])
        self.data = data_transposed
        print("Datos cargados")
        self.historial += "Carga de datos.\n"

    def tt_split(self, random_state=666, test_size=0.25):
        """
        Subdivide el conjunto de datos en training y test
        manteniendo las proporciones originales,
        a la hora de evaluar el test error habrá que ver
        si pesarlo en función de la clase o no.
        """
        a, b, c, d = train_test_split(
            self.data.drop('class', axis=1),
            self.data.loc[:, 'class'],
            test_size=test_size,
            random_state=random_state,
            stratify=self.data.loc[:, 'class']
        )
        self.xtrain['ori'] = a
        self.xtest['ori'] = b
        self.ytrain['ori'] = c
        self.ytest['ori'] = d
        print("Train test split realizado.")
        self.historial += "tt_split con" + str(test_size) + ".\n"

    def balance(self, method="smote", random_state=666):
        """
        Utiliza diferentes algoritmos de oversampling de
        la muestra minoritaria para balancear las clases.
        Por defecto es SMOTE.
        """
        valid = {"smote", "svmsmote", "adasyn", "random"}
        if method not in valid:
            raise ValueError(
                "Method not implemented yet. Use 'smote' or 'svmsmote'"
                )
        X = self.xtrain['ori']
        y = self.ytrain['ori']
        print('Original dataset shape %s' % Counter(y))
        if method == "smote":
            over = SMOTE(random_state=random_state)
            x_res, y_res = over.fit_resample(X, y)
        elif method == "svmsmote":
            over = SVMSMOTE(random_state=random_state)
            x_res, y_res = over.fit_resample(X, y)
        elif method == "adasyn":
            over = ADASYN(random_state=random_state)
            x_res, y_res = over.fit_resample(X, y)
        else:
            over = RandomOverSampler(random_state=random_state)
            x_res, y_res = over.fit_resample(X, y)
        print('Method: %s \nResampled dataset shape %s' % (
            method, Counter(y_res)))
        self.xtrain['bal'] = x_res
        self.ytrain['bal'] = y_res
        self.xtest['bal'] = self.xtest['ori']
        self.ytest['bal'] = self.ytest['ori']
        self.historial += "Balanceo con " + method.upper() + ".\n"

    def scale(self):
        """
        Aplica una estandarización de los datos
        para poderles aplicar ciertos métodos,
        entre otros PCA.
        """
        scaling = StandardScaler()
        self.xtrain['esc'] = scaling.fit_transform(self.xtrain['bal'])
        self.xtest['esc'] = scaling.fit_transform(self.xtest['bal'])
        self.ytrain['esc'] = self.ytrain['bal']
        self.ytest['esc'] = self.ytest['bal']
        self.historial += "Datos escalados.\n"
        print("Datos escalados.")

    def reduce_dimensionality(
            self,
            random_state=666,
            threshold=0.9):
        pca = PCA(
            n_components=threshold,
            random_state=random_state
        )
        """
        Sobre datos estandarizados se aplica una PCA
        y se extraen las características que explican
        un nivel de varianza indicado en threshold.
        """
        xtrain_pca = pca.fit_transform(self.xtrain['esc'])
        xtest_pca = pca.transform(self.xtest['esc'])
        resultado = str(xtrain_pca.shape[1]) + " características explican" + \
            " el " + str(100*round(sum(pca.explained_variance_ratio_), 1)) + \
            f" % de la varianza."
        print(resultado)
        self.historial += "PCA: " + resultado + "\n"
        self.xtrain['pca'], self.xtest['pca'] = xtrain_pca, xtest_pca
        self.ytrain['pca'] = self.ytrain['esc']
        self.ytest['pca'] = self.ytest['esc']

    def model(self, model, over, random_state=666):
        if model not in self.models:
            raise ValueError(
                "Check genetic.models for available models."
                )

        def save_metrics(y, ypred, model):
            print("MODELO: " + model.upper() + ".\n")
            print(classification_report(
                y, y_pred, target_names=self.labels
            ))
            self.metrics[model] = classification_report(
                y, y_pred, target_names=self.labels,
                output_dict=True, zero_division=1
            )

        y, y_pred = mod[model](
            xtrain=self.xtrain,
            xtest=self.xtest,
            ytrain=self.ytrain,
            ytest=self.ytest,
            rs=random_state,
            over=over)
        save_metrics(y, y_pred, model)


if __name__ == "__main__":
    gene = genetic()
    gene.load_data("./data/golub.csv")
    gene.tt_split()
    gene.balance()
    gene.scale()
    gene.reduce_dimensionality()
    """ gene.model("zero-rules", over='ori')
    gene.model("logreg", over='ori')
    gene.model("svm", over='ori')
    gene.model("rf", over='ori')"""
    #gene.model("gbc", over='ori')
    gene.model("gbc", over='bal')
    #gene.model("xgb", over='esc')
    #gene.model("xgb", over='pca')
