""" Tarea de Ciencia de datos para Biomedicina
Carlos Hernani Morales
gitlab::carhermo
correo::carhermo@protonmail.com;carhermo@alumni.uv.es
2019/2020

Modelos implementados

Python 3.7.5
Linter used: pycodestyle """


import numpy as np
from os import cpu_count
from parameter_grid import grid
from sklearn.model_selection import GridSearchCV
from parameter_grid import grid
# Classification Libraries
from sklearn.cluster import KMeans
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from xgboost import XGBClassifier
NJOBS = int(cpu_count()/2)


def zer(over,
        xtrain=None,
        xtest=None,
        ytrain=None,
        ytest=None,
        rs=None,
        n_jobs=NJOBS):
    """
    Predice la clase mayoritaria.
    Por tanto lo hacemos sobre el conjunto original sin
    balancear.
    """
    y = ytest[over]
    ypred = ytest[over].mode()[0]*np.ones(ytest[over].shape, dtype=int)
    return [y, ypred]


def lgr(over,
        xtrain=None,
        xtest=None,
        ytrain=None,
        ytest=None,
        rs=None,
        n_jobs=NJOBS):
    estimator = LogisticRegression(solver="liblinear", random_state=rs)
    gcv = GridSearchCV(
        estimator=estimator, param_grid=grid['logreg'], cv=3,
        scoring='accuracy', verbose=3
        )
    gcv.fit(xtrain[over], ytrain[over])
    best_model = gcv.best_estimator_
    y = ytest[over]
    ypred = best_model.predict(xtest[over])
    return [y, ypred]


def svm(over,
        xtrain=None,
        xtest=None,
        ytrain=None,
        ytest=None,
        rs=None,
        n_jobs=NJOBS):
    estimator = SVC(random_state=rs)
    gcv = GridSearchCV(
        estimator=estimator, param_grid=grid['svm'], cv=3,
        verbose=3
        )
    gcv.fit(xtrain[over], ytrain[over])
    best_model = gcv.best_estimator_
    y = ytest[over]
    ypred = best_model.predict(xtest[over])
    return [y, ypred]


def rfo(over,
        xtrain=None,
        xtest=None,
        ytrain=None,
        ytest=None,
        rs=None,
        n_jobs=NJOBS):
    estimator = RandomForestClassifier(random_state=rs)
    gcv = GridSearchCV(
        estimator=estimator, param_grid=grid['rf'], cv=3,
        scoring='accuracy', verbose=3
        )
    gcv.fit(xtrain[over], ytrain[over])
    best_model = gcv.best_estimator_
    y = ytest[over]
    ypred = best_model.predict(xtest[over])
    return [y, ypred]


def gbc(over,
        xtrain=None,
        xtest=None,
        ytrain=None,
        ytest=None,
        rs=None,
        n_jobs=NJOBS):
    estimator = GradientBoostingClassifier(random_state=rs)
    gcv = GridSearchCV(
        estimator=estimator, param_grid=grid['gbc'], cv=3,
        scoring='accuracy', verbose=3
    )
    gcv.fit(xtrain[over], ytrain[over])
    best_model = gcv.best_estimator_
    y = ytest[over]
    ypred = best_model.predict(xtest[over])
    return [y, ypred]


def xgb(over,
        xtrain=None,
        xtest=None,
        ytrain=None,
        ytest=None,
        rs=None,
        n_jobs=NJOBS):
    estimator = XGBClassifier(random_state=rs, n_jobs=NJOBS)
    gcv = GridSearchCV(
        estimator=estimator, param_grid=grid['xgb'], cv=3,
        scoring='accuracy', verbose=3
    )
    gcv.fit(xtrain[over], ytrain[over])
    best_model = gcv.best_estimator_
    y = ytest[over]
    ypred = best_model.predict(xtest[over])
    return [y, ypred]


mod = {
    "zero-rules": zer,
    "logreg": lgr,
    "svm": svm,
    "rf": rfo,
    "gbc": gbc,
    "xgb": xgb
}
