""" Tarea de Ciencia de datos para Biomedicina
Carlos Hernani Morales
gitlab::carhermo
correo::carhermo@protonmail.com;carhermo@alumni.uv.es
2019/2020

Grid de parámetros para el gridsearch de algunos métodos

Python 3.7.5
Linter used: pycodestyle """


grid = {
    "logreg": {
        "C": [1e-03, 1e-2, 1e-1, 1, 10],
        "penalty": ['l1', 'l2']
        },
    "svm": {
        "C": [0.1, 1, 10, 100],
        "gamma": [1, 0.1, 0.01, 0.001, 0.00001, 10],
        "kernel": ['linear', 'rbf', 'poly'],
        "decision_function_shape": ['ovo', 'ovr']
        },
    "rf": {
        "bootstrap": [False, True],
        "n_estimators": [60, 70, 80, 90, 100],
        "max_features": [0.6, 0.65, 0.7, 0.75, 0.8],
        "min_samples_leaf": [8, 10, 12, 14],
        "min_samples_split": [3, 5, 7]
        },
    "xgb": {
        "max_depth": [3, 4, 5, 6, 7, 8, 10, 12],
        "min_child_weight": [1, 2, 4, 6, 8, 10, 12, 15],
        "n_estimators": [40, 50, 60, 70, 80, 90, 100, 110, 120, 130],
        "learning_rate": [0.001, 0.01, 0.05, 0.1, 0.2, 0.3]
        },
    "gbc": {
        "loss": ['deviance', 'exponential'],
        "max_depth": [3, 4, 5, 6, 7, 8, 10, 12],
        "min_samples_leaf": [1, 2, 4, 6, 8, 10, 12, 15],
        "n_estimators": [40, 50, 60, 70, 80, 90, 100, 110, 120, 130],
        "learning_rate": [0.001, 0.01, 0.05, 0.1, 0.2, 0.3]
        }
}
